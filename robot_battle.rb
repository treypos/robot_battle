@arr1 = Array.new(3, 100)
@arr2 = Array.new(3, 100)

def attack(arr)
  sleep 1
  i = rand(0..2)
  arr[i] = arr[i] - rand(30..100)
  if arr[i] > 0
  puts "У Робота #{i + 1} осталось #{arr[i]} работоспособности"
  elsif arr[i] <= 0
  puts "Робот #{i + 1} уничтожен..."
  end
sleep 1
end

def victory?
  robots_left1 = @arr1.count {|x| x >= 1}
  robots_left2 = @arr2.count {|x| x >= 1}
  if robots_left1 == 0
  puts "Команда 2 победила, осталось живых роботов #{robots_left2} #{@arr2}"
  return true
  end
  if robots_left2 == 0
  puts "Команда 1 победила, осталось живых роботов #{robots_left1} #{@arr1}"
  return true
  end
  false
end

def stats
  cnt1 = @arr1.count{|x| x > 0}
  cnt2 = @arr2.count{|x| x > 0}
  puts "В 1 команде #{cnt1} роботов в строю #{@arr1}"
  puts "Во 2 команде #{cnt2} роботов в строю #{@arr2}"
end

loop do
  puts "Первая команда наносит удар..."
  attack(@arr2)
  exit if victory?
  stats
  sleep 2
  puts

  puts "Вторая команда наносит удар..."
  attack(@arr1)
  exit if victory?
  stats
  sleep 2
  puts
end